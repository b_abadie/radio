BIN_DIR=/usr/bin
APP_DIR=/usr/share/applications
SYSTEMD_DIR=/usr/lib/systemd/user

.PHONY: dist
dist:
	tar -c $(wildcard bin/*) $(wildcard applications/*) Makefile | gzip -n > radio-$(shell git describe --tags).tar.gz

.PHONY: install
install:
	for bin in $(wildcard bin/*); do install -Dm755 $$bin $(DESTDIR)$(BIN_DIR)/$$(basename $$bin); done
	for app in $(wildcard applications/*); do install -Dm 644 $$app $(DESTDIR)$(APP_DIR)/$$(basename $$app); done
	for srv in $(wildcard services/*); do install -Dm 644 $$srv $(DESTDIR)$(SYSTEMD_DIR)/$$(basename $$srv); done
